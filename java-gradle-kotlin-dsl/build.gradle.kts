plugins {
  application
}

application {
  mainClassName = "com.gitlab.security_products.tests.App"
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("junit:junit:4.12")
    implementation("io.netty:netty:3.9.1.Final")
    implementation("org.apache.maven:maven-artifact:3.3.9")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.9.2")
    implementation("org.mozilla:rhino:1.7.10")
    implementation("org.apache.geode:geode-core:1.1.1")
}
