# Dependency Scanning demo project

The purpose of this project is to demo Dependency Scanning for all supported frameworks and languages. These are represented by the subdirectories of this project which were taken from the secure [tests subgroup](https://gitlab.com/gitlab-org/security-products/tests).

The locations that show the Dependency Scanning features are:
- vulnerability report: https://gitlab.com/gitlab-org/security-products/tests/dependency-scanning/-/security/vulnerability_report
- dependency list: https://gitlab.com/gitlab-org/security-products/tests/dependency-scanning/-/dependencies
- remediation is available on `yarn` vulnerabilities such as: [this vulnerability](https://gitlab.com/gitlab-org/security-products/tests/dependency-scanning/-/security/vulnerabilities/15529258) (click the "resolve with..." button at the top right corner of the screen)

## Updating
 
To add a newly supported language or framework add a sub-directory to this project's source.

### Java and Python projects

Java and Python projects scan the first found directory only, thus the Dependency Scanning config for these projects has to handle each subdirectory separately. You will find the configuration for scanning java and python in [.gitlab/java-and-scala.yml](https://gitlab.com/gitlab-org/security-products/tests/dependency-scanning/-/blob/main/.gitlab/java-and-scala.yml) and [.gitlab/python.yml](https://gitlab.com/gitlab-org/security-products/tests/dependency-scanning/-/blob/main/.gitlab/python.yml). Adding a Java or Python project requires a change in one or the other config.
